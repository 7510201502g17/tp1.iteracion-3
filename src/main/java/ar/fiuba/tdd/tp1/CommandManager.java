package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.commands.GetValueCommand;
import ar.fiuba.tdd.tp1.commands.ReversibleCommand;

import java.util.Stack;

/**
 * Created by ezequiel on 11/10/15.
 */
public class CommandManager {

    private Stack<ReversibleCommand> commandsToUndo = new Stack<>();
    private Stack<ReversibleCommand> commandsToRedo = new Stack<>();

    public String executeCommand(ReversibleCommand command) {
        //command.execute();
        if (command instanceof GetValueCommand) {
            return command.execute();
        }
        commandsToUndo.add(command);
        commandsToRedo.clear();
        return command.execute();
    }

    public void undo() {
        if ( !commandsToUndo.isEmpty()) {
            ReversibleCommand command = commandsToUndo.pop();
            command.undo();
            commandsToRedo.add(command);
        }
    }

    public void redo() {
        if (!commandsToRedo.isEmpty()) {
            ReversibleCommand command = commandsToRedo.pop();
            command.redo();
            commandsToUndo.add(command);
        }
    }
}
