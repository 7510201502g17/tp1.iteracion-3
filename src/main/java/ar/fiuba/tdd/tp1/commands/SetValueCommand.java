package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.contents.IContent;

/**
 * Created by ezequiel on 11/10/15.
 */
public class SetValueCommand implements ReversibleCommand {

    private final Spreadsheet spreadsheet;
    private final String cell;
    private final String value;
    private IContent content;
    private String workSheetName;

    public SetValueCommand(Spreadsheet spreadsheet, String workSheetName,String cell, String value) {
        this.spreadsheet = spreadsheet;
        this.cell = cell;
        this.value = value;
        this.workSheetName = workSheetName;
    }

    @Override
    public String execute() {
        Sheet sheet = spreadsheet.getSheet(this.workSheetName);
        this.content = sheet.getCell(cell).getContent();
        spreadsheet.setCellValue(sheet.getName(), cell, value);
        return null;
    }

    @Override
    public void undo() {
        spreadsheet.getCurrentSheet().getCell(cell).setNewContent(content);
    }

    @Override
    public void redo() {
        execute();
    }
}
