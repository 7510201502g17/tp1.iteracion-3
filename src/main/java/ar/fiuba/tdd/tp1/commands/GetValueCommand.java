package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

/**
 * Created by pc on 12/10/15.
 */
public class GetValueCommand implements ReversibleCommand {
    private final Spreadsheet spreadsheet;
    private final String cell;

    public GetValueCommand(Spreadsheet spreadsheet , String cell) {
        this.cell = cell;
        this.spreadsheet = spreadsheet;
    }

    @Override
    public String execute() {
        String name = spreadsheet.getCurrentSheet().getName();
        DependencyHelper.startCycleController(cell);
        String value;
        value = spreadsheet.getValueAsString(name, cell);
        if ( DependencyHelper.cycleFound()) {
            throw new BadReferenceException();
        }
        return value;
    }

    @Override
    public void undo() {

    }

    @Override
    public void redo() {

    }
}
