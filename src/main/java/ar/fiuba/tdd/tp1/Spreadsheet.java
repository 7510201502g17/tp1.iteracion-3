package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.acceptance.driver.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ezequiel on 26/09/15.
 */
public class Spreadsheet{

    Map<String, Sheet> sheets = new HashMap<>();
    private Sheet currentSheet;
    String name;


    private static final String generalCellPattern = "^(!\\w+\\.)?[A-z][0-9]+$";
    //private static final String cellPattern = "^[A-z][0-9]+$";
    private static final String otherSheetCellPattern = "^(!\\w+\\.)[A-z][0-9]+$";
    private static String tableFormat = "%10s";

    public Spreadsheet(String name) {
        this.name = name;
        Sheet sheet = new Sheet("default");
        this.addSheet(sheet);
    }

    public void importSpreadsheet(SpreadsheetJson spreadsheetJson ) {
        this.name = spreadsheetJson.getName();

        ArrayList<CellJsonFormat> cells = spreadsheetJson.getCells();

        Iterator<CellJsonFormat> iterator = cells.iterator();
        while ( iterator.hasNext()) {
            CellJsonFormat cell = iterator.next();
            String sheet = cell.getSheet();
            if ( !sheets.containsKey(sheet) ) {
                this.addSheet(new Sheet(sheet));
            }
            this.setCellValue(cell.getSheet(),cell.getId(),cell.getValue());

        }
    }

    public ArrayList<Cell> getCells() {

        ArrayList<Cell> arrayList = new ArrayList<>();
        for (String key : sheets.keySet()) {
            arrayList.addAll(sheets.get(key).getCells());
        }
        return arrayList;

    }


    public Integer size() {
        return sheets.size();
    }

    public void addSheet(Sheet sheet) {
        sheets.put(sheet.getName(), sheet);
        this.currentSheet = sheet;
    }

    public Sheet getSheet(String sheetName) {
        return sheets.get(sheetName);
    }

    public void setCurrentSheet(String name) {
        this.validateSheetName(name);
        this.currentSheet = this.sheets.get(name);
    }

    public Sheet getCurrentSheet() {
        return this.currentSheet;
    }

    public float getValueFromCellInSheet(String workSheetName,String cellId) {
        this.validateSheetName(workSheetName);
        setCurrentSheet(workSheetName);
        return this.getValueFromCell(cellId);
    }

    public float getValueFromCell(String cellPosition) {
        String result;
        validateCell(cellPosition);
        if (Pattern.matches(otherSheetCellPattern, cellPosition)) {
            result = this.getCellFromAnotherSheet(cellPosition).getContent().getValue();
        } else {
            result = this.currentSheet.getCell(cellPosition).getContent().getValue();
        }
        if (result.isEmpty()) {
            return 0;
        }
        return Float.parseFloat(result);
    }

    private boolean validateCell(String cellPosition) {
        if (!Pattern.matches(generalCellPattern, cellPosition)) {
            throw new BadReferenceException();
        }
        return true;
    }

    private Cell getCellFromAnotherSheet(String cellPosition) throws IllegalArgumentException {
        String sheetName = cellPosition.replaceAll("\\.[A-z][0-9]+$","").replaceAll("^!","");
        String newCellPosition = cellPosition.replaceAll("^(!\\w+\\.)","");
        Sheet sheet = this.sheets.get(sheetName);
        if (sheet == null) {
            throw new IllegalArgumentException("Invalid sheet name");
        }
        return this.sheets.get(sheetName).getCell(newCellPosition);
    }

    public List<String> getNames() {
        return new ArrayList<>(sheets.keySet());
    }

    public void setCellValue(String workSheetName, String cellId, String value) {
        Sheet sheet = this.validateSheetName(workSheetName);
        validateCell(cellId);
        DependencyHelper dependencyHelper = new DependencyHelper(this, sheet, cellId);
        sheets.get(workSheetName).putValue(cellId, Parser.stringToContent(value, dependencyHelper));
    }

    public String getValueAsString(String workSheetName, String cellId) {
        /*
        if (graph != null) {
            graph.addVertex(cellId);
        }*/
        this.validateSheetName(workSheetName);
        this.validateCell(cellId);
        Cell cell = getCell(workSheetName,cellId);
        if (cell.getType().equals("String")) {
            return cell.getContent().getRaw();
        }
        return getCell(workSheetName,cellId).getRaw();
    }



    private Cell getCell(String workSheetName, String cellId) {
        if (Pattern.matches(otherSheetCellPattern, cellId)) {
            return this.getCellFromAnotherSheet(cellId);
        } else {
            return sheets.get(workSheetName).getCell(cellId);
        }
    }

    private Sheet validateSheetName(String workSheetName) {
        Sheet sheet = this.sheets.get(workSheetName);
        if (sheet == null) {
            throw new UndeclaredWorkSheetException();
        }
        return sheet;
    }

    public void removeSheet(String sheetName) {
        sheets.remove(sheetName);
    }

    public List<String> getSheetsNames() {
        return new ArrayList<>(sheets.keySet());
    }

    public void printSheet() {
        List<Cell> cells = this.currentSheet.getCells();
        char maxColumn = 'A';
        int maxLine = 1;
        for (Cell cell : cells) {
            String cellId = cell.getID();
            char column = cellId.charAt(0);
            int line = Integer.parseInt(cellId.substring(1));
            if (column > maxColumn) {
                maxColumn = column;
            }
            if (line > maxLine) {
                maxLine = line;
            }
        }
        print(maxColumn,maxLine);
    }

    private void print(char maxColumn, int maxLine) {
        System.out.format(tableFormat,"");
        for (char i = 'A'; i <= maxColumn; i++) {
            System.out.format(tableFormat,i );
        }
        for (int j = 1; j <= maxLine; j++) {
            System.out.println();
            System.out.format(tableFormat,j);
            for (char i = 'A'; i <= maxColumn; i++) {
                String cellPosition = i + Integer.toString(j);
                System.out.format(tableFormat, getValueAsString(currentSheet.getName(), cellPosition));
            }
        }
    }
}
