package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.*;
import ar.fiuba.tdd.tp1.commands.AddSheetCommand;
import ar.fiuba.tdd.tp1.commands.GetValueCommand;
import ar.fiuba.tdd.tp1.commands.SetValueCommand;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pc on 5/10/15.
 */
public class SpreadSheetDriver implements SpreadSheetTestDriver {

    private List<String> workBooksNames = new ArrayList<>();
    Map<String, Spreadsheet> spreadsheets = new HashMap<>();
    CommandManager commandManager = new CommandManager();

    @Override
    public List<String> workBooksNames() {
        return workBooksNames;
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        Spreadsheet spreadsheet = new Spreadsheet(name);
        spreadsheets.put(name, spreadsheet);
        workBooksNames.add(name);
        Sheet sheet = new Sheet("default");
        spreadsheet.addSheet(sheet);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        Spreadsheet spreadsheet = spreadsheets.get(workbookName);
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,name);
        commandManager.executeCommand(addSheetCommand);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        validateSpreadsheetName(workBookName);
        Spreadsheet spreadSheet = this.spreadsheets.get(workBookName);
        return spreadSheet.getNames();
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        validateSpreadsheetName(workBookName);
        Spreadsheet spreadsheet = spreadsheets.get(workBookName);
        SetValueCommand setValueCommand = new SetValueCommand(spreadsheet,workSheetName,cellId,value);
        //spreadsheet.setValueInCell(workSheetName,cellId,value);
        commandManager.executeCommand(setValueCommand);
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        validateSpreadsheetName(workBookName);
        try {
            return this.spreadsheets.get(workBookName).getValueAsString(workSheetName, cellId);
        } catch (BadReferenceException e) {
            return "Error:BAD_FORMULA";
        } catch (NotFormulaException e) {
            return this.spreadsheets.get(workBookName).getValueAsString(workSheetName,cellId);
        }

    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        validateSpreadsheetName(workBookName);
        //commandManager.executeCommand(new GetValueCommand(this.spreadsheets.get(workBookName),cellId));
        try {
            String result = commandManager.executeCommand(new GetValueCommand(this.spreadsheets.get(workBookName),cellId));
            return Double.parseDouble(result);
            // /return this.spreadsheets.get(workBookName).getValueFromCellInSheet(workSheetName,cellId);
        } catch (BadReferenceException e) {
            throw new BadReferenceException();
        } catch (Exception e) {
            throw new BadFormulaException();
        }
    }

    public Spreadsheet validateSpreadsheetName(String workBookName) {
        Spreadsheet spreadsheet = this.spreadsheets.get(workBookName);
        if (spreadsheet == null) {
            throw new UndeclaredWorkSheetException();
        }
        return spreadsheet;
    }

    @Override
    public void undo() {
        commandManager.undo();

    }

    @Override
    public void redo() {
        commandManager.redo();

    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        validateSpreadsheetName(workBookName);
        Spreadsheet spreadsheet = spreadsheets.get(workBookName);
        Sheet sheet = spreadsheet.getSheet(workSheetName);
        Cell cell = sheet.getCell(cellId);
        if ( formatter.equals("symbol") ) {
            MoneyFormatter mf = new MoneyFormatter(format,0);
            cell.setFormatter(mf);
        } else if ( formatter.equals("format") ) {
            DateFormatter dateFormatter = new DateFormatter(format);
            cell.setFormatter(dateFormatter);
        } else if ( formatter.equals("decimal") ) {
            MoneyFormatter formatter1 = (MoneyFormatter) cell.getFormatter();
            String symbol = formatter1.getMoneyType();
            MoneyFormatter moneyFormatter = new MoneyFormatter(symbol,Integer.parseInt(format));
            cell.setFormatter(moneyFormatter);
        }

    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        validateSpreadsheetName(workBookName);
        Spreadsheet spreadsheet = spreadsheets.get(workBookName);
        Sheet sheet = spreadsheet.getSheet(workSheetName);
        Cell cell = sheet.getCell(cellId);
        cell.setCellType(type);
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        JsonWriter jsonWriter = new JsonWriter(fileName);
        try {
            jsonWriter.write(spreadsheets.get(workBookName));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        JsonWriter jsonWriter = new JsonWriter(fileName);
        Spreadsheet spreadsheet = new Spreadsheet("");
        try {
            SpreadsheetJson spreadsheetJson = jsonWriter.read();
            spreadsheet.importSpreadsheet(spreadsheetJson);
            if ( spreadsheets.containsKey(spreadsheetJson.getName())) {
                spreadsheets.remove(spreadsheetJson.getName());
            }
            this.spreadsheets.put(spreadsheetJson.getName(),spreadsheet);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {

        CsvHandler csv = new CsvHandler(path);
        Spreadsheet spreadsheet =  validateSpreadsheetName(workBookName);
        spreadsheet.setCurrentSheet(sheetName);
        try {
            csv.write(spreadsheet);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {

        CsvHandler csv = new CsvHandler(path);
        Spreadsheet spreadsheet =  validateSpreadsheetName(workBookName);
        List<String> sheets = spreadsheet.getSheetsNames();
        for (String sheet :sheets) {
            spreadsheet.removeSheet(sheet);
        }
        try {
            Spreadsheet spreadsheet1 = csv.read(sheetName);
            Sheet sheet = spreadsheet1.getSheet(sheetName);
            spreadsheet.addSheet(sheet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return spreadsheets.size();
    }

    public void printSpreadsheets() {
        for (String string : workBooksNames) {
            System.out.println(string);
        }
    }

    public void setCurrentSheet(String currentSpreadsheet, String name) {
        spreadsheets.get(currentSpreadsheet).setCurrentSheet(name);
    }

    public void printSheets(String currentSpreadsheet) {
        List<String> sheetNames = spreadsheets.get(currentSpreadsheet).getSheetsNames();
        for (String name : sheetNames) {
            System.out.println(name);
        }
    }

    public String getStringCellValue(String currentSpreadsheet, String cellPosition) {
        return getCellValueAsString(currentSpreadsheet, spreadsheets.get(currentSpreadsheet).getCurrentSheet().getName(), cellPosition);
    }

    public void setValueInCell(String currentSpreadsheet, String cellPosition, String cellValue) {
        setCellValue(currentSpreadsheet, spreadsheets.get(currentSpreadsheet).getCurrentSheet().getName(), cellPosition, cellValue);
    }

    public void printSheet(String currentSpreadsheet) {
        spreadsheets.get(currentSpreadsheet).printSheet();
    }
}
