package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by fran on 09/10/15.
 */
public class Parser {

    private static final Pattern rangePattern = Pattern.compile("^(!\\w+\\.)?[A-z][0-9]+:[A-z][0-9]+$");

    public static IContent stringToContent(String value, DependencyHelper dependencyHelper) {

        if (value.matches("-?\\d+(\\.\\d+)?")) {
            return new NumericContent(Float.parseFloat(value));
        }

        if (value.startsWith("=")) {
            return new FormulaContent(value, dependencyHelper);
        }

        return new StringContent(value);
    }

    public static boolean validateRange(String range) {
        Matcher matcher = rangePattern.matcher(range);
        return matcher.matches();
    }

    public static String floatToString(float value) {
        if (value == (long) value) {
            return String.format("%d", (long) value);
        } else {
            return String.format("%s",value);
        }
    }







}
