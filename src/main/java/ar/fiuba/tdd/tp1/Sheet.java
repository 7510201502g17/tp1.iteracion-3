package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.IContent;

import java.util.*;

/**
 * Created by ezequiel on 26/09/15.
 */
public class Sheet {
    private Map<String, Cell> cellGrid = new HashMap<String, Cell>();
    private String name;

    public Sheet() {
        this.name = "default";
    }

    public Sheet(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public Cell getCell(String cellPosition) {
        Cell cell = cellGrid.get(cellPosition);
        if (cell == null) {
            cell = new Cell(this.name, cellPosition);
            cellGrid.put(cellPosition, cell);
        }
        return cell;
    }

    public void putValue(String cellPosition, IContent content) {
        Cell cell = this.getCell(cellPosition);
        cell.setNewContent(content);
        cellGrid.put(cellPosition, cell);
    }

    public ArrayList<Cell> getCells() {
        ArrayList<Cell> array = new ArrayList<>();

        for (String key : cellGrid.keySet()) {
            array.add(cellGrid.get(key));
        }

        return array;
    }
}