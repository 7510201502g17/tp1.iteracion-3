package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.EmptyContent;
import ar.fiuba.tdd.tp1.contents.IContent;
import ar.fiuba.tdd.tp1.formatter.IFormatter;

/**
 * Created by ezequiel on 27/09/15.
 */
public class Cell implements Comparable<Cell> {
    String sheet;
    String id;
    IContent value = new EmptyContent();
    IFormatter formatter = null;
    String type;

    public Cell(String sheet, String id) {
        this.sheet = sheet;
        this.id = id;
        this.type = "";

    }

    public void setCellType(String type) {
        this.type = type;
    }

    public void setFormatter(IFormatter formatter) {

        this.formatter = formatter;
    }

    public String getType() {
        return this.type;
    }

    public IFormatter getFormatter() {
        return this.formatter;
    }

    public String getSheet() {
        return this.sheet;
    }

    public String getID() {
        return this.id;
    }

    public IContent getContent() {
        return value;
    }

    public void setNewContent(IContent newContent) {
        value = newContent;
    }

    public String getRaw() {

        if (this.formatter == null) {
            return getContent().getValue();
        }
        return formatter.format(getContent().getValue());
    }


    @Override
    public int compareTo(Cell other) {
        if (this.getID().charAt(1) < other.getID().charAt(1)) {
            return -1;
        } else if (this.getID().charAt(1) > other.getID().charAt(1)) {
            return 1;
        } else {
            if (this.getID().charAt(0) < other.getID().charAt(0)) {
                return -1;
            }
        }
        return 1;
    }

    @Override
    public boolean equals(Object other) {
        return other == this;

    }
}
