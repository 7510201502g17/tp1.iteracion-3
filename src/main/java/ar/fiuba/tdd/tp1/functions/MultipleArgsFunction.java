package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MultipleArgsFunction implements Function {
    
    private static final Pattern argsPattern = Pattern.compile("^[^(),]+(,[^(),]+)+");
    protected String[] args;
 
    protected void saveArgs(String argument) {
        Matcher matcher = argsPattern.matcher(argument);
        if (!matcher.find()) {
            throw new IllegalConcatArgumentException();
        }
        args = argument.split(",");
    }
    
    protected String evalString(String string, DependencyHelper helper) {
        String str = "";
        try {
            str = helper.getValueAsString(string);
        } catch (BadReferenceException badReferenceException) {
            str = string;
        }
        return str;
    }
}
