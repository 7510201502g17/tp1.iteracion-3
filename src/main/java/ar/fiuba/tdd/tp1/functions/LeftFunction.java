package ar.fiuba.tdd.tp1.functions;

public class LeftFunction extends StringFunction{

    @Override
    protected String transformString(String str, int index) {
        try {
            return str.substring(0, index);
        } catch (StringIndexOutOfBoundsException e) {
            throw e;
        }
    }
}