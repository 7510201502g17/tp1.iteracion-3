package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

/**
 * Created by fran on 16/10/15.
 */
public class ConcatFunction extends MultipleArgsFunction {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalConcatArgumentException {
        saveArgs(argument);
        StringBuffer result = new StringBuffer();
        for (String oneArgument : args) {
            result.append(concat(oneArgument,helper));
        }
        return result.toString();
    }

    private String concat(String argument,DependencyHelper helper) {
        StringBuffer buf = new StringBuffer();
        buf.append(evalString(argument, helper));
        return buf.toString();
    }

}
