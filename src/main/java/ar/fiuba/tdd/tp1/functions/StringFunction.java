package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

public abstract class StringFunction extends MultipleArgsFunction {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        saveArgs(argument);
        String str = evalString(args[0],  helper);
        int index = Integer.parseInt(args[1]);
        return transformString(str, index);
    }

    protected abstract String transformString(String str, int index);

}
