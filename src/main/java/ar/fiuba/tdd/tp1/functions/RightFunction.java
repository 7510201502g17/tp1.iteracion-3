package ar.fiuba.tdd.tp1.functions;

public class RightFunction extends StringFunction {

    @Override
    protected String transformString(String str, int index) {
        try {
            return str.substring(str.length() - index, str.length());
        } catch (StringIndexOutOfBoundsException e) {
            throw e;
        }
    }
}