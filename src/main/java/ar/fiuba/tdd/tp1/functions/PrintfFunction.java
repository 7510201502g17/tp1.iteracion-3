package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

public class PrintfFunction extends MultipleArgsFunction implements Function {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        saveArgs(argument);
        String src = args[0];
        for (int i = 1; i < args.length; i++) {
            String res = this.evalString(args[i], helper);
            src = src.replace("$" + Integer.toString(i - 1), res);
        }
        return src;
    }

}
