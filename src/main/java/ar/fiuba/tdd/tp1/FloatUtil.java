package ar.fiuba.tdd.tp1;

/**
 * Created by ezequiel on 25/10/15.
 */
public class FloatUtil {
    public static String toString(double number) {
        if (number == (long) number) {
            return String.format("%d", (long) number);
        } else {
            return String.format("%s", number);
        }
    }
}
