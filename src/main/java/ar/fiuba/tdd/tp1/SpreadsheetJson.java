package ar.fiuba.tdd.tp1;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by pc on 28/10/15.
 */
public class SpreadsheetJson {

    @Expose
    String name;
    @Expose
    String version;
    @Expose
    ArrayList<CellJsonFormat> cells;

    public SpreadsheetJson(String name,String version,ArrayList<CellJsonFormat> cells) {
        this.name = name;
        this.version = version;
        this.cells = cells;

    }

    public ArrayList<CellJsonFormat> getCells() {
        return this.cells;
    }

    public String getName() {
        return this.name;
    }



}
