package ar.fiuba.tdd.tp1;

import com.google.gson.*;

import java.io.*;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by pc on 17/10/15.
 */
public class JsonWriter {
    private String fileName;
    private GsonBuilder builder;
    private Gson gson;

    public JsonWriter(String fileName) {
        this.fileName = fileName;
        this.builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting();
        //this.builder.registerTypeAdapter(IContent.class,new IContentAdapter());
        this.gson = builder.create();
    }

    public void write(Spreadsheet spreadsheet) throws IOException {

        ArrayList<CellJsonFormat> cells = new ArrayList<>();
        ArrayList<Cell> spreadsheetCells = spreadsheet.getCells();
        Iterator<Cell> iter = spreadsheetCells.iterator();
        while (iter.hasNext()) {
            Cell cell = iter.next();
            CellJsonFormat cellJson = new CellJsonFormat(cell);
            cells.add(cellJson);
        }

        SpreadsheetJson spreadsheetJson = new SpreadsheetJson(spreadsheet.name,"1.1",cells);


        String json = gson.toJson(spreadsheetJson);
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(fileName),"UTF-8");
        out.write(json);
        out.close();

    }

    public SpreadsheetJson read() throws IOException {
        //Type typeObject = new TypeToken<ArrayList<Cell>>(){}.getType();
        //BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));

        InputStreamReader input = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(input);
        SpreadsheetJson spreadsheetJson = gson.fromJson(bufferedReader,SpreadsheetJson.class);
        return spreadsheetJson;
    }
}