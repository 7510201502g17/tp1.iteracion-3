package ar.fiuba.tdd.tp1.formatter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by pc on 28/10/15.
 */
public class NumberFormatter implements IFormatter {

    @Expose
    @SerializedName("Number.Decimal") private int decimals;
    private DecimalFormat decimalFormat;

    public NumberFormatter(int decimals) {
        this.decimals = decimals;
        this.decimalFormat = new DecimalFormat();

        DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols();
        decimalSymbol.setDecimalSeparator('.');

        decimalFormat.setGroupingUsed(false);
        decimalFormat.setDecimalFormatSymbols(decimalSymbol);
        decimalFormat.setMinimumFractionDigits(this.decimals);
        decimalFormat.setMaximumFractionDigits(this.decimals);
    }

    @Override
    public String format(String string) {
        Double value;
        try {
            value = Double.parseDouble(string);
        } catch (NumberFormatException e) {
            return "Error:BAD_CURRENCY";
        }
        return decimalFormat.format(value);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getFormatValue() {
        return String.valueOf(this.decimals);
    }
}
