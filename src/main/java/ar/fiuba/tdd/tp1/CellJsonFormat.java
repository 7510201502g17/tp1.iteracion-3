package ar.fiuba.tdd.tp1;

import com.google.gson.annotations.Expose;

import ar.fiuba.tdd.tp1.formatter.*;

/**
 * Created by pc on 28/10/15.
 */
public class CellJsonFormat {

    @Expose
    private String sheet;
    @Expose
    private String id;
    @Expose
    private String value;
    @Expose
    private FormatJson formatter;

    public CellJsonFormat(Cell cell) {
        this.id = cell.getID();
        this.sheet = cell.getSheet();
        this.value = cell.getRaw();
        IFormatter cellFormatter = cell.getFormatter();
        formatter = new FormatJson();
        if (cellFormatter != null ) {
            if (cellFormatter instanceof DateFormatter) {
                formatter.setDate(cellFormatter.getFormatValue());
            } else if (cellFormatter instanceof MoneyFormatter) {
                formatter.setMoney(cellFormatter.getFormatValue());
            } else if (cellFormatter instanceof NumberFormatter) {
                formatter.setDecimals(cellFormatter.getFormatValue());
            }
        }
    }

    public String getSheet() {
        return this.sheet;
    }

    public String getId() {
        return this.id;
    }

    public String getValue() {
        return this.value;
    }
}
