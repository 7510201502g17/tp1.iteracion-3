package ar.fiuba.tdd.tp1.contents;

import ar.fiuba.tdd.tp1.FloatUtil;
import ar.fiuba.tdd.tp1.NotFormulaException;
import ar.fiuba.tdd.tp1.functions.*;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ezequiel on 28/09/15.
 */
public class FormulaContent implements IContent {
    private final DependencyHelper dependencyHelper;
    String formula;

    private static final Pattern formulaPattern = Pattern.compile("[^,(+\\-=]*\\([^()]*\\)");
    private HashMap<String, Function> formulas;

    private static final String average = "AVERAGE";
    private static final String min = "MIN";
    private static final String max = "MAX";
    private static final String concat = "CONCAT";
    private static final String today = "TODAY";
    private static final String left = "LEFT";
    private static final String right = "RIGHT";
    private static final String printf = "PRINTF";
    private String actualCellsaver;

    public FormulaContent(String newFormula, DependencyHelper dependencyHelper) {
        this.formula = newFormula;
        this.formulas = new HashMap<>();
        this.formulas.put(average, new AverageFunction());
        this.formulas.put(min, new MinFunction());
        this.formulas.put(max, new MaxFunction());
        this.formulas.put(concat, new ConcatFunction());
        this.formulas.put(today, new TodayFunction());
        this.formulas.put(left, new LeftFunction());
        this.formulas.put(right, new RightFunction());
        this.formulas.put(printf, new PrintfFunction());
        this.dependencyHelper = dependencyHelper;

    }

    @Override
    public String getValue() throws IllegalArgumentException {
        return resolveFormula(formula);
    }

    @Override
    public String getRaw() {
        return formula;
    }

    private String resolveFormula(String formula) throws NotFormulaException {
        if (!formula.startsWith("=")) {
            throw new NotFormulaException();
        }
        formula = formula.substring(1, formula.length());
        return checkFormulas(formula);
    }

    private String checkFormulas(String formula) {
        boolean found = true;
        boolean isString = false;
        String value = "";
        while (found) {
            found = false;
            Matcher matcher = formulaPattern.matcher(formula);
            while (matcher.find()) {
                found = true;
                value = resolveFunction(matcher.group(0));
                formula = formula.replace(matcher.group(0), value);
                isString = isString(value);
            }
        }
        return solveCheckedFormula(value, isString, formula);
    }

    private boolean isString(String value) {
        boolean isString = false;
        try {
            Float.parseFloat(value);
        } catch (Exception e) {
            isString = true;
        }
        return isString;
    }

    private String resolveFunction(String possibleFormula) {
        String formulaName = possibleFormula.replaceFirst("\\([^()]*\\)", "");
        String argument = possibleFormula.substring(formulaName.length() + 1,
                possibleFormula.length() - 1);
        formulaName = formulaName.replaceAll(" ", "");
        if (this.formulas.containsKey(formulaName)) {
            return this.formulas.get(formulaName).operate(argument, dependencyHelper);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private String solveCheckedFormula(String value, boolean isString, String formula) {
        if (isString) {
            if (formula.compareTo(value) != 0) {
                throw new IllegalArgumentException();
            }
            return formula;
        }
        actualCellsaver = DependencyHelper.getActualCell();
        formula = formula.replace(" ", "");
        return FloatUtil.toString(parseSum(formula));
    }

    private float parseSum(String formula) {
        float rightResult = 0;
        int iterator = 1;
        char operator = '+';
        int beginIndex = 0;
        formula = cleanFormula(formula);
        while (iterator <= formula.length()) {
            if (iterator == formula.length() || formula.charAt(iterator) == '+'
                    || formula.charAt(iterator) == '-') {
                float leftResult = parseTerm(formula.substring(beginIndex, iterator));
                rightResult = resolveSum(rightResult, operator, leftResult);
                if (iterator < formula.length()) {
                    operator = formula.charAt(iterator);
                }
                beginIndex = iterator + 1;
            }
            iterator++;
        }
        return rightResult;
    }

    private float resolveSum(float rightResult, char operator, float leftResult) {
        if (operator == '+') {
            rightResult += leftResult;
        } else {
            rightResult -= leftResult;
        }
        return rightResult;
    }

    private String cleanFormula(String formula) {
        formula = formula.replace("--", "+");
        formula = formula.replace("+-", "-");
        formula = formula.replace("++", "+");
        formula = formula.replace("-+", "-");
        return formula;
    }

    private float parseTerm(String formula) {
        String[] values = formula.split("\\^");
        float result = validate(values[0]);
        for (int i = 1; i < values.length; i++) {
            float pow = validate(values[i]);
            result = (float) Math.pow((double) result, (double) pow);
        }
        return result;
    }

    private float validate(String term) {
        try {
            return Float.parseFloat(term);
        } catch (IllegalArgumentException e) {
            if (term.isEmpty()) {
                return 0;
            }
            float value = dependencyHelper.getValueFromCell(term);
            DependencyHelper.setActualCell(actualCellsaver);
            return value;
        }
    }
}
