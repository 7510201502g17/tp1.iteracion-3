package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.commands.GetValueCommand;
import ar.fiuba.tdd.tp1.commands.SetValueCommand;

import java.io.*;
import java.util.*;


/**
 * Created by pc on 20/10/15.
 */
public class CsvHandler {
    public String fileName;

    public CsvHandler(String fileName) {
        this.fileName = fileName;
    }

    public void write(Spreadsheet spreadsheet) throws IOException {
        List<Cell> cells = spreadsheet.getCurrentSheet().getCells();
        Collections.sort(cells);
        Iterator<String> iterator = getCellsIds(cells).iterator();
        CommandManager commandManager = new CommandManager();
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(fileName),"UTF-8");

        int oldColumn = 0;
        int oldRow = 0;

        while ( iterator.hasNext() ) {
            String cellId = iterator.next();
            int column = getColumnPosition(cellId);
            int row = getRowPosition(cellId);
            boolean newRowAdded = addRowsToActual(out, row, oldRow);
            if (newRowAdded) {
                oldColumn = 0;
            }
            addColumnsToActual(out, column, oldColumn);
            out.write(commandManager.executeCommand(new GetValueCommand(spreadsheet,cellId)));
            oldColumn = column;
            oldRow = row;
        }
        out.close();
    }

    private void addColumnsToActual(OutputStreamWriter out, int column, int oldColumn) throws IOException {
        while (oldColumn < column) {
            out.write(",");
            oldColumn++;
        }
    }

    private boolean addRowsToActual(OutputStreamWriter out, int row, int oldRow) throws IOException {
        boolean newRowAdded = false;
        while (oldRow < row) {
            newRowAdded = true;
            out.write("\n");
            oldRow++;
        }
        return newRowAdded;
    }

    private int getColumnPosition(String cellId) {
        char actualLetter = cellId.charAt(0);
        char firstLetter = 'A';
        int pos =  actualLetter - firstLetter;
        return pos;
    }

    private int getRowPosition(String cellId) {
        char actualLetter = cellId.charAt(1);
        char firstLetter = '1';
        int pos =  actualLetter - firstLetter;
        return pos;
    }

    private List<String> getCellsIds(List<Cell> cells) {
        Iterator<Cell> iterator = cells.iterator();
        List<String> cellsIds = new ArrayList<>();
        while ( iterator.hasNext() ) {
            Cell cell = iterator.next();
            cellsIds.add(cell.getID());
        }
        return cellsIds;
    }

    public Spreadsheet read(String name) throws IOException {
        Spreadsheet spreadsheet = new Spreadsheet("");
        spreadsheet.addSheet(new Sheet(name));
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
        CommandManager commandManager = new CommandManager();

        String line;
        int row = 0;
        String cellId;
        String sheet = spreadsheet.getCurrentSheet().getName();

        while ( (line = br.readLine()) != null ) {
            row++;
            char column = 'A';
            for (String cellValue : line.split(",")) {
                if (!cellValue.isEmpty()) {
                    cellId = column + Integer.toString(row);
                    commandManager.executeCommand(new SetValueCommand(spreadsheet,sheet,cellId,cellValue));
                }
                column++;
            }
        }
        br.close();

        return spreadsheet;
    }
}
