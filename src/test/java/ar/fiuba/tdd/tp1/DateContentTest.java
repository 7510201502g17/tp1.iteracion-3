package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.DateContent;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;
/**
 * Created by pc on 22/10/15.
 */
public class DateContentTest {


    @Test
    public void getValue() throws ParseException {
        DateContent content = new DateContent("dd/MM/yyyy","21/10/2015");
        assertEquals("21/10/2015",content.getValue());

    }

    @Test
    public void changeFormatDate() throws ParseException {
        DateContent content = new DateContent("dd/MM/yyyy","21/10/2015");
        content.changeFormat("MM/dd/yyyy");

        assertEquals("10/21/2015",content.getValue());

        content.changeFormat("yyyy/MM/dd");

        assertEquals("2015/10/21",content.getValue());


    }
}
