package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * Created by pc on 28/10/15.
 */
public class FormatterTest {

    @Test
    public void moneyTest() {
        MoneyFormatter moneyFormatter = new MoneyFormatter("$" , 2);

        assertEquals("$ 2.00" , moneyFormatter.format("2"));

        MoneyFormatter moneyFormatter1 = new MoneyFormatter("$" , 1);
        assertEquals("$ 5.3" , moneyFormatter1.format("5.300"));
    }

    @Test
    public void dateTest() {
        DateFormatter dateFormatter = new DateFormatter("YYYY-MM-DD");
        String oneDate = "2012-07-14T00:00:00Z";

        assertEquals("2012-07-14",dateFormatter.format(oneDate));

        dateFormatter = new DateFormatter("DD-MM-YYYY");

        assertEquals("14-07-2012" , dateFormatter.format(oneDate));

        dateFormatter = new DateFormatter("MM-DD-YYYY");

        assertEquals("07-14-2012" , dateFormatter.format(oneDate));
    }



}
