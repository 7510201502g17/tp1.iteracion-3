package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SpreadsheetTest {
    private static final double DELTA = 0.00001;

    @Test
    public void createSpreadsheet() {
        Object spreadsheet = new Spreadsheet("");
        assertNotNull(spreadsheet);
    }

    @Test
    public void addingNewSheetToSpreadsheet() {
        Spreadsheet spreadsheet = new Spreadsheet("");
        Integer sheetsQuantity = spreadsheet.size();
        assertEquals(1,sheetsQuantity, DELTA);
        Sheet sheet = new Sheet("other");
        spreadsheet.addSheet(sheet);
        spreadsheet.getCurrentSheet();
        sheetsQuantity = spreadsheet.size();
        assertEquals(2, sheetsQuantity, DELTA);
    }

    @Test(expected = BadReferenceException.class)
    public void invalidCell() {
        Spreadsheet spreadsheet = new Spreadsheet("");
        spreadsheet.getValueFromCell("AF1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidSheetName() {
        Spreadsheet spreadsheet = new Spreadsheet("");
        spreadsheet.getValueFromCell("!pepe.A1");
    }

    @Test(expected = BadReferenceException.class)
    public void invalidSetCellValue() {
        Spreadsheet spreadsheet = new Spreadsheet("");
        spreadsheet.setCellValue("default","98","5");
    }

}
