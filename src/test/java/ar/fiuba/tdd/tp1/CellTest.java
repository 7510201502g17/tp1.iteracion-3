package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.IContent;
import ar.fiuba.tdd.tp1.contents.NumericContent;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellTest {
    private static final double DELTA = 0.00001;

    @Test
    public void getCellNewValueIsZero() {
        Cell cell = new Cell("default","A1");
        assertEquals("",cell.getContent().getValue());
    }

    @Test
    public void setContent() {
        Cell cell = new Cell("default","A1");
        IContent content = new NumericContent(5);
        cell.setNewContent(content);
        assertEquals(5,Float.parseFloat(cell.getContent().getValue()), DELTA);
    }



}
